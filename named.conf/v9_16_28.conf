options {
	answer-cookie true;
	automatic-interface-scan yes;
	bindkeys-file "/tmp/prefix/etc/bind.keys";
#	blackhole {none;};
	cookie-algorithm siphash24;
	coresize default;
	datasize default;
#	directory <none>
	dnssec-policy "none";
	dump-file "named_dump.db";
	edns-udp-size 1232;
	files unlimited;
	geoip-directory "/usr/share/GeoIP";
	heartbeat-interval 60;
	interface-interval 60;
#	keep-response-order {none;};
	listen-on {any;};
	listen-on-v6 {any;};
#	lock-file "/tmp/prefix/var/run/named/named.lock";
	match-mapped-addresses no;
	max-ixfr-ratio unlimited;
	max-rsa-exponent-size 0; /* no limit */
	max-udp-size 1232;
	memstatistics-file "named.memstats";
	nocookie-udp-size 4096;
	notify-rate 20;
	nta-lifetime 3600;
	nta-recheck 300;
#	pid-file "/tmp/prefix/var/run/named/named.pid"; 
	port 53;
	reuseport yes;
	prefetch 2 9;
	recursing-file "named.recursing";
	recursive-clients 1000;
	request-nsid false;
	reserved-sockets 512;
	resolver-query-timeout 10;
	rrset-order { order random; };
	secroots-file "named.secroots";
	send-cookie true;
	serial-query-rate 20;
	server-id none;
	session-keyalg hmac-sha256;
#	session-keyfile "/tmp/prefix/var/run/named/session.key";
	session-keyname local-ddns;
	stacksize default;
	startup-notify-rate 20;
	statistics-file "named.stats";
	tcp-advertised-timeout 300;
	tcp-clients 150;
	tcp-idle-timeout 300;
	tcp-initial-timeout 300;
	tcp-keepalive-timeout 300;
	tcp-listen-queue 10;
#	tkey-dhkey <none>
#	tkey-domain <none>
#	tkey-gssapi-credential <none>
	transfer-message-size 20480;
	transfers-in 10;
	transfers-out 10;
	transfers-per-ns 2;
	trust-anchor-telemetry yes;

	/* view */
	allow-new-zones no;
	allow-notify {none;};
	allow-query-cache { localnets; localhost; };
	allow-query-cache-on { any; };
	allow-recursion { localnets; localhost; };
	allow-recursion-on { any; };
	allow-update-forwarding {none;};
	auth-nxdomain false;
	check-dup-records warn;
	check-mx warn;
	check-names primary fail;
	check-names response ignore;
	check-names secondary warn;
	check-spf warn;
	clients-per-query 10;
	dnssec-accept-expired no;
	dnssec-validation auto; 
	fetch-quota-params 100 0.1 0.3 0.7;
	fetches-per-server 0;
	fetches-per-zone 0;
	glue-cache yes;
	lame-ttl 0;
	lmdb-mapsize 32M;
	max-cache-size 90%;
	max-cache-ttl 604800; /* 1 week */
	max-clients-per-query 100;
	max-ncache-ttl 10800; /* 3 hours */
	max-recursion-depth 7;
	max-recursion-queries 100;
	max-stale-ttl 86400; /* 1 day */
	message-compression yes;
	min-ncache-ttl 0; /* 0 hours */
	min-cache-ttl 0; /* 0 seconds */
	minimal-any false;
	minimal-responses no-auth-recursive;
	notify-source *;
	notify-source-v6 *;
	nsec3-test-zone no;
	parental-source *;
	parental-source-v6 *;
	provide-ixfr true;
	qname-minimization relaxed;
	query-source address *;
	query-source-v6 address *;
	recursion true;
	request-expire true;
	request-ixfr true;
	require-server-cookie no;
	resolver-nonbackoff-tries 3;
	resolver-retry-interval 800; /* in milliseconds */
	root-key-sentinel yes;
	servfail-ttl 1;
#	sortlist <none>
	stale-answer-client-timeout off;
	stale-answer-enable false;
	stale-answer-ttl 30; /* 30 seconds */
	stale-cache-enable true;
	stale-refresh-time 30; /* 30 seconds */
	synth-from-dnssec no;
#	topology <none>
	transfer-format many-answers;
	v6-bias 50;
	zero-no-soa-ttl-cache no;

	/* zone */
	allow-query {any;};
	allow-query-on {any;};
	allow-transfer {any;};
#	also-notify <none>
	alt-transfer-source *;
	alt-transfer-source-v6 *;
	check-integrity yes;
	check-mx-cname warn;
	check-sibling yes;
	check-srv-cname warn;
	check-wildcard yes;
	dialup no;
	dnssec-dnskey-kskonly no;
	dnssec-loadkeys-interval 60;
	dnssec-secure-to-insecure no;
	dnssec-update-mode maintain;
#	forward <none>
#	forwarders <none>
#	inline-signing no;
	ixfr-from-differences false;
	max-journal-size default;
	max-records 0;
	max-refresh-time 2419200; /* 4 weeks */
	max-retry-time 1209600; /* 2 weeks */
	max-transfer-idle-in 60;
	max-transfer-idle-out 60;
	max-transfer-time-in 120;
	max-transfer-time-out 120;
	min-refresh-time 300;
	min-retry-time 500;
	multi-master no;
	notify yes;
	notify-delay 5;
	notify-to-soa no;
	serial-update-method increment;
	sig-signing-nodes 100;
	sig-signing-signatures 10;
	sig-signing-type 65534;
	sig-validity-interval 30; /* days */
	dnskey-sig-validity 0; /* default: sig-validity-interval */
	transfer-source *;
	transfer-source-v6 *;
	try-tcp-refresh yes; /* BIND 8 compat */
	update-check-ksk yes;
	zero-no-soa-ttl yes;
	zone-statistics terse;
};
#
#  Zones in the "_bind" view are NOT counted in the count of zones.
#
view "_bind" chaos {
	recursion no;
	notify no;
	allow-new-zones no;
	max-cache-size 2M;

	# Prevent use of this zone in DNS amplified reflection DoS attacks
	rate-limit {
		responses-per-second 3;
		slip 0;
		min-table-size 10;
	};

	zone "version.bind" chaos {
		type primary;
		database "_builtin version";
	};

	zone "hostname.bind" chaos {
		type primary;
		database "_builtin hostname";
	};

	zone "authors.bind" chaos {
		type primary;
		database "_builtin authors";
	};

	zone "id.server" chaos {
		type primary;
		database "_builtin id";
	};
};
#
#  Default trusted key(s), used if 
# "dnssec-validation auto;" is set and
#  /tmp/prefix/etc/bind.keys doesn't exist).
#
# BEGIN TRUST ANCHORS
# The bind.keys file is used to override the built-in DNSSEC trust anchors
# which are included as part of BIND 9.  The only trust anchors it contains
# are for the DNS root zone (".").  Trust anchors for any other zones MUST
# be configured elsewhere; if they are configured here, they will not be
# recognized or used by named.
#
# To use the built-in root key, set "dnssec-validation auto;" in the
# named.conf options, or else leave "dnssec-validation" unset.  If
# "dnssec-validation" is set to "yes", then the keys in this file are
# ignored; keys will need to be explicitly configured in named.conf for
# validation to work.  "auto" is the default setting, unless named is
# built with "configure --disable-auto-validation", in which case the
# default is "yes".
#
# This file is NOT expected to be user-configured.
#
# Servers being set up for the first time can use the contents of this file
# as initializing keys; thereafter, the keys in the managed key database
# will be trusted and maintained automatically.
#
# These keys are current as of Mar 2019.  If any key fails to initialize
# correctly, it may have expired.  In that event you should replace this
# file with a current version.  The latest version of bind.keys can always
# be obtained from ISC at https://www.isc.org/bind-keys.
#
# See https://data.iana.org/root-anchors/root-anchors.xml for current trust
# anchor information for the root zone.

trust-anchors {
        # This key (20326) was published in the root zone in 2017.
        . initial-key 257 3 8 "AwEAAaz/tAm8yTn4Mfeh5eyI96WSVexTBAvkMgJzkKTOiW1vkIbzxeF3
                +/4RgWOq7HrxRixHlFlExOLAJr5emLvN7SWXgnLh4+B5xQlNVz8Og8kv
                ArMtNROxVQuCaSnIDdD5LKyWbRd2n9WGe2R8PzgCmr3EgVLrjyBxWezF
                0jLHwVN8efS3rCj/EWgvIWgb9tarpVUDK/b58Da+sqqls3eNbuv7pr+e
                oZG+SrDK6nWeL3c6H5Apxz7LjVc1uTIdsIXxuOLYA4/ilBmSVIzuDWfd
                RUfhHdY6+cn8HFRm+2hM8AnXGXws9555KrUB5qihylGa8subX2Nn6UwN
                R1AkUTV74bU=";
};
# END TRUST ANCHORS

primaries _default_iana_root_zone_primaries {
	2001:500:200::b;	# b.root-servers.net
	2001:500:2::c;		# c.root-servers.net
	2001:500:2f::f;		# f.root-servers.net
	2001:500:12::d0d;	# g.root-servers.net
	2001:7fd::1;		# k.root-servers.net
	2620:0:2830:202::132;	# xfr.cjr.dns.icann.org
	2620:0:2d0:202::132;	# xfr.lax.dns.icann.org
	199.9.14.201;		# b.root-servers.net
	192.33.4.12;		# c.root-servers.net
	192.5.5.241;		# f.root-servers.net
	192.112.36.4;		# g.root-servers.net
	193.0.14.129;		# k.root-servers.net
	192.0.47.132;		# xfr.cjr.dns.icann.org
	192.0.32.132;		# xfr.lax.dns.icann.org
};

