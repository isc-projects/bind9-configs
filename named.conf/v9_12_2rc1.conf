options {
	answer-cookie true;
	automatic-interface-scan yes;
	bindkeys-file "/tmp/prefix/etc/bind.keys";
#	blackhole {none;};
	cookie-algorithm aes;
	coresize default;
	datasize default;
#	deallocate-on-exit <obsolete>;
#	directory <none>
	dump-file "named_dump.db";
	edns-udp-size 4096;
#	fake-iquery <obsolete>;
	files unlimited;
#	has-old-clients <obsolete>;
	heartbeat-interval 60;
#	host-statistics <obsolete>;
	interface-interval 60;
#	keep-response-order {none;};
	listen-on {any;};
	listen-on-v6 {any;};
#	lock-file "/tmp/prefix/var/run/named/named.lock";
	match-mapped-addresses no;
	max-rsa-exponent-size 0; /* no limit */
	max-udp-size 4096;
	memstatistics-file "named.memstats";
#	multiple-cnames <obsolete>;
#	named-xfer <obsolete>;
	nocookie-udp-size 4096;
	notify-rate 20;
	nta-lifetime 3600;
	nta-recheck 300;
#	pid-file "/tmp/prefix/var/run/named/named.pid"; 
	port 53;
	prefetch 2 9;
	random-device none;
	recursing-file "named.recursing";
	recursive-clients 1000;
	request-nsid false;
	reserved-sockets 512;
	resolver-query-timeout 10;
	secroots-file "named.secroots";
	send-cookie true;
#	serial-queries <obsolete>;
	serial-query-rate 20;
	server-id none;
	session-keyalg hmac-sha256;
#	session-keyfile "/tmp/prefix/var/run/named/session.key";
	session-keyname local-ddns;
	stacksize default;
	startup-notify-rate 20;
	statistics-file "named.stats";
#	statistics-interval <obsolete>;
	tcp-advertised-timeout 300;
	tcp-clients 150;
	tcp-idle-timeout 300;
	tcp-initial-timeout 300;
	tcp-keepalive-timeout 300;
	tcp-listen-queue 10;
#	tkey-dhkey <none>
#	tkey-domain <none>
#	tkey-gssapi-credential <none>
	transfer-message-size 20480;
	transfers-in 10;
	transfers-out 10;
	transfers-per-ns 2;
#	treat-cr-as-space <obsolete>;
	trust-anchor-telemetry yes;
#	use-id-pool <obsolete>;
#	use-ixfr <obsolete>;

	/* view */
	allow-new-zones no;
	allow-notify {none;};
	allow-query-cache { localnets; localhost; };
	allow-query-cache-on { any; };
	allow-recursion { localnets; localhost; };
	allow-recursion-on { any; };
	allow-update-forwarding {none;};
#	allow-v6-synthesis <obsolete>;
	auth-nxdomain false;
	check-dup-records warn;
	check-mx warn;
	check-names master fail;
	check-names response ignore;
	check-names slave warn;
	check-spf warn;
	cleaning-interval 0;  /* now meaningless */
	clients-per-query 10;
	dnssec-accept-expired no;
	dnssec-enable yes;
	dnssec-validation yes; 
#	fetch-glue <obsolete>;
	fetch-quota-params 100 0.1 0.3 0.7;
	fetches-per-server 0;
	fetches-per-zone 0;
	filter-aaaa-on-v4 no;
	filter-aaaa-on-v6 no;
	filter-aaaa { any; };
	glue-cache yes;
	lame-ttl 600;
	lmdb-mapsize 32M;
	max-cache-size 90%;
	max-cache-ttl 604800; /* 1 week */
	max-clients-per-query 100;
	max-ncache-ttl 10800; /* 3 hours */
	max-recursion-depth 7;
	max-recursion-queries 75;
	max-stale-ttl 604800; /* 1 week */
	message-compression yes;
#	min-roots <obsolete>;
	minimal-any false;
	minimal-responses no-auth-recursive;
	notify-source *;
	notify-source-v6 *;
	nsec3-test-zone no;
	provide-ixfr true;
	query-source address *;
	query-source-v6 address *;
	recursion true;
	request-expire true;
	request-ixfr true;
	require-server-cookie no;
	resolver-nonbackoff-tries 3;
	resolver-retry-interval 800; /* in milliseconds */
#	rfc2308-type1 <obsolete>;
	root-key-sentinel yes;
	servfail-ttl 1;
#	sortlist <none>
	stale-answer-enable false;
	stale-answer-ttl 1; /* 1 second */
	synth-from-dnssec yes;
#	topology <none>
	transfer-format many-answers;
	v6-bias 50;
	zero-no-soa-ttl-cache no;

	/* zone */
	allow-query {any;};
	allow-query-on {any;};
	allow-transfer {any;};
#	also-notify <none>
	alt-transfer-source *;
	alt-transfer-source-v6 *;
	check-integrity yes;
	check-mx-cname warn;
	check-sibling yes;
	check-srv-cname warn;
	check-wildcard yes;
	dialup no;
	dnssec-dnskey-kskonly no;
	dnssec-loadkeys-interval 60;
	dnssec-secure-to-insecure no;
	dnssec-update-mode maintain;
#	forward <none>
#	forwarders <none>
	inline-signing no;
	ixfr-from-differences false;
#	maintain-ixfr-base <obsolete>;
#	max-ixfr-log-size <obsolete>
	max-journal-size default;
	max-records 0;
	max-refresh-time 2419200; /* 4 weeks */
	max-retry-time 1209600; /* 2 weeks */
	max-transfer-idle-in 60;
	max-transfer-idle-out 60;
	max-transfer-time-in 120;
	max-transfer-time-out 120;
	min-refresh-time 300;
	min-retry-time 500;
	multi-master no;
	notify yes;
	notify-delay 5;
	notify-to-soa no;
	serial-update-method increment;
	sig-signing-nodes 100;
	sig-signing-signatures 10;
	sig-signing-type 65534;
	sig-validity-interval 30; /* days */
	transfer-source *;
	transfer-source-v6 *;
	try-tcp-refresh yes; /* BIND 8 compat */
	update-check-ksk yes;
	zero-no-soa-ttl yes;
	zone-statistics terse;
};
#
#  Zones in the "_bind" view are NOT counted in the count of zones.
#
view "_bind" chaos {
	recursion no;
	notify no;
	allow-new-zones no;

	# Prevent use of this zone in DNS amplified reflection DoS attacks
	rate-limit {
		responses-per-second 3;
		slip 0;
		min-table-size 10;
	};

	zone "version.bind" chaos {
		type master;
		database "_builtin version";
	};

	zone "hostname.bind" chaos {
		type master;
		database "_builtin hostname";
	};

	zone "authors.bind" chaos {
		type master;
		database "_builtin authors";
	};

	zone "id.server" chaos {
		type master;
		database "_builtin id";
	};
};
#
#  Default trusted key(s), used if 
# "dnssec-validation auto;" is set and
#  sysconfdir/bind.keys doesn't exist).
#
# BEGIN MANAGED KEYS
# The bind.keys file is used to override the built-in DNSSEC trust anchors
# which are included as part of BIND 9.  The only trust anchors it contains
# are for the DNS root zone (".").  Trust anchors for any other zones MUST
# be configured elsewhere; if they are configured here, they will not be
# recognized or used by named.
#
# The built-in trust anchors are provided for convenience of configuration.
# They are not activated within named.conf unless specifically switched on.
# To use the built-in key, use "dnssec-validation auto;" in the
# named.conf options.  Without this option being set, the keys in this
# file are ignored.
#
# This file is NOT expected to be user-configured.
#
# These keys are current as of October 2017.  If any key fails to
# initialize correctly, it may have expired.  In that event you should
# replace this file with a current version.  The latest version of
# bind.keys can always be obtained from ISC at https://www.isc.org/bind-keys.
#
# See https://data.iana.org/root-anchors/root-anchors.xml
# for current trust anchor information for the root zone.

managed-keys {
        # This key (19036) is to be phased out starting in 2017. It will
        # remain in the root zone for some time after its successor key
        # has been added. It will remain this file until it is removed from
        # the root zone.
        . initial-key 257 3 8 "AwEAAagAIKlVZrpC6Ia7gEzahOR+9W29euxhJhVVLOyQbSEW0O8gcCjF
                FVQUTf6v58fLjwBd0YI0EzrAcQqBGCzh/RStIoO8g0NfnfL2MTJRkxoX
                bfDaUeVPQuYEhg37NZWAJQ9VnMVDxP/VHL496M/QZxkjf5/Efucp2gaD
                X6RS6CXpoY68LsvPVjR0ZSwzz1apAzvN9dlzEheX7ICJBBtuA6G3LQpz
                W5hOA2hzCTMjJPJ8LbqF6dsV6DoBQzgul0sGIcGOYl7OyQdXfZ57relS
                Qageu+ipAdTTJ25AsRTAoub8ONGcLmqrAmRLKBP1dfwhYB4N7knNnulq
                QxA+Uk1ihz0=";

        # This key (20326) was published in the root zone in 2017.
        # Servers which were already using the old key (19036) should
        # roll seamlessly to this new one via RFC 5011 rollover. Servers
        # being set up for the first time can use the contents of this
        # file as initializing keys; thereafter, the keys in the
        # managed key database will be trusted and maintained
        # automatically.
        . initial-key 257 3 8 "AwEAAaz/tAm8yTn4Mfeh5eyI96WSVexTBAvkMgJzkKTOiW1vkIbzxeF3
                +/4RgWOq7HrxRixHlFlExOLAJr5emLvN7SWXgnLh4+B5xQlNVz8Og8kv
                ArMtNROxVQuCaSnIDdD5LKyWbRd2n9WGe2R8PzgCmr3EgVLrjyBxWezF
                0jLHwVN8efS3rCj/EWgvIWgb9tarpVUDK/b58Da+sqqls3eNbuv7pr+e
                oZG+SrDK6nWeL3c6H5Apxz7LjVc1uTIdsIXxuOLYA4/ilBmSVIzuDWfd
                RUfhHdY6+cn8HFRm+2hM8AnXGXws9555KrUB5qihylGa8subX2Nn6UwN
                R1AkUTV74bU=";
};
# END MANAGED KEYS

