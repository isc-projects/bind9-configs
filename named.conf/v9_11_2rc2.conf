options {
	automatic-interface-scan yes;
	bindkeys-file "/tmp/prefix/etc/bind.keys";
#	blackhole {none;};
	cookie-algorithm aes;
	coresize default;
	datasize default;
	files unlimited;
	stacksize default;
#	session-keyfile "/tmp/prefix/var/run/named/session.key";
	session-keyname local-ddns;
	session-keyalg hmac-sha256;
#	deallocate-on-exit <obsolete>;
#	directory <none>
	dump-file "named_dump.db";
#	fake-iquery <obsolete>;
#	has-old-clients <obsolete>;
	heartbeat-interval 60;
#	host-statistics <obsolete>;
	interface-interval 60;
#	keep-response-order {none;};
	listen-on {any;};
	listen-on-v6 {any;};
	match-mapped-addresses no;
	max-rsa-exponent-size 0; /* no limit */
	memstatistics-file "named.memstats";
#	multiple-cnames <obsolete>;
#	named-xfer <obsolete>;
	nta-lifetime 3600;
	nta-recheck 300;
	notify-rate 20;
#	pid-file "/tmp/prefix/var/run/named/named.pid"; /* or /lwresd.pid */
#	lock-file "/tmp/prefix/var/run/named/named.lock";
	port 53;
	prefetch 2 9;
	recursing-file "named.recursing";
	secroots-file "named.secroots";
	random-device "/dev/random";
	recursive-clients 1000;
	resolver-query-timeout 10;
	rrset-order { order random; };
#	serial-queries <obsolete>;
	serial-query-rate 20;
	server-id none;
	startup-notify-rate 20;
	statistics-file "named.stats";
#	statistics-interval <obsolete>;
	tcp-clients 150;
	tcp-listen-queue 10;
#	tkey-dhkey <none>
#	tkey-gssapi-credential <none>
#	tkey-domain <none>
	transfer-message-size 20480;
	transfers-per-ns 2;
	transfers-in 10;
	transfers-out 10;
#	treat-cr-as-space <obsolete>;
	trust-anchor-telemetry yes;
#	use-id-pool <obsolete>;
#	use-ixfr <obsolete>;
	edns-udp-size 4096;
	max-udp-size 4096;
	nocookie-udp-size 4096;
	send-cookie true;
	request-nsid false;
	reserved-sockets 512;

	/* DLV */
	dnssec-lookaside . trust-anchor dlv.isc.org;

	/* view */
	allow-notify {none;};
	allow-update-forwarding {none;};
	allow-query-cache { localnets; localhost; };
	allow-query-cache-on { any; };
	allow-recursion { localnets; localhost; };
	allow-recursion-on { any; };
#	allow-v6-synthesis <obsolete>;
#	sortlist <none>
#	topology <none>
	auth-nxdomain false;
	minimal-any false;
	minimal-responses false;
	recursion true;
	provide-ixfr true;
	request-ixfr true;
	request-expire true;
#	fetch-glue <obsolete>;
#	rfc2308-type1 <obsolete>;
	additional-from-auth true;
	additional-from-cache true;
	query-source address *;
	query-source-v6 address *;
	notify-source *;
	notify-source-v6 *;
	cleaning-interval 0;  /* now meaningless */
#	min-roots <obsolete>;
	lame-ttl 600;
	servfail-ttl 1;
	max-ncache-ttl 10800; /* 3 hours */
	max-cache-ttl 604800; /* 1 week */
	transfer-format many-answers;
	max-cache-size 90%;
	check-names master fail;
	check-names slave warn;
	check-names response ignore;
	check-dup-records warn;
	check-mx warn;
	check-spf warn;
	acache-enable no;
	acache-cleaning-interval 60;
	max-acache-size 16M;
	dnssec-enable yes;
	dnssec-validation yes; 
	dnssec-accept-expired no;
	fetches-per-zone 0;
	fetch-quota-params 100 0.1 0.3 0.7;
	clients-per-query 10;
	max-clients-per-query 100;
	max-recursion-depth 7;
	max-recursion-queries 75;
	zero-no-soa-ttl-cache no;
	nsec3-test-zone no;
	allow-new-zones no;
	lmdb-mapsize 32M;
	fetches-per-server 0;
	require-server-cookie no;
	v6-bias 50;
	message-compression yes;
	/* zone */
	allow-query {any;};
	allow-query-on {any;};
	allow-transfer {any;};
	notify yes;
#	also-notify <none>
	notify-delay 5;
	notify-to-soa no;
	dialup no;
#	forward <none>
#	forwarders <none>
#	maintain-ixfr-base <obsolete>;
#	max-ixfr-log-size <obsolete>
	transfer-source *;
	transfer-source-v6 *;
	alt-transfer-source *;
	alt-transfer-source-v6 *;
	max-transfer-time-in 120;
	max-transfer-time-out 120;
	max-transfer-idle-in 60;
	max-transfer-idle-out 60;
	max-records 0;
	max-retry-time 1209600; /* 2 weeks */
	min-retry-time 500;
	max-refresh-time 2419200; /* 4 weeks */
	min-refresh-time 300;
	multi-master no;
	dnssec-secure-to-insecure no;
	sig-validity-interval 30; /* days */
	sig-signing-nodes 100;
	sig-signing-signatures 10;
	sig-signing-type 65534;
	inline-signing no;
	zone-statistics terse;
	max-journal-size unlimited;
	ixfr-from-differences false;
	check-wildcard yes;
	check-sibling yes;
	check-integrity yes;
	check-mx-cname warn;
	check-srv-cname warn;
	zero-no-soa-ttl yes;
	update-check-ksk yes;
	serial-update-method increment;
	dnssec-update-mode maintain;
	dnssec-dnskey-kskonly no;
	dnssec-loadkeys-interval 60;
	try-tcp-refresh yes; /* BIND 8 compat */
};
#
#  Zones in the "_bind" view are NOT counted in the count of zones.
#
view "_bind" chaos {
	recursion no;
	notify no;
	allow-new-zones no;

	# Prevent use of this zone in DNS amplified reflection DoS attacks
	rate-limit {
		responses-per-second 3;
		slip 0;
		min-table-size 10;
	};

	zone "version.bind" chaos {
		type master;
		database "_builtin version";
	};

	zone "hostname.bind" chaos {
		type master;
		database "_builtin hostname";
	};

	zone "authors.bind" chaos {
		type master;
		database "_builtin authors";
	};

	zone "id.server" chaos {
		type master;
		database "_builtin id";
	};
};
#
#  Default trusted key(s) for builtin DLV support
#  (used if "dnssec-lookaside auto;" is set and
#  sysconfdir/bind.keys doesn't exist).
#
# BEGIN MANAGED KEYS
# The bind.keys file is used to override the built-in DNSSEC trust anchors
# which are included as part of BIND 9.  As of the current release, the only
# trust anchors it contains are those for the DNS root zone ("."), and for
# the ISC DNSSEC Lookaside Validation zone ("dlv.isc.org").  Trust anchors
# for any other zones MUST be configured elsewhere; if they are configured
# here, they will not be recognized or used by named.
#
# The built-in trust anchors are provided for convenience of configuration.
# They are not activated within named.conf unless specifically switched on.
# To use the built-in root key, set "dnssec-validation auto;" in
# named.conf options.  To use the built-in DLV key, set
# "dnssec-lookaside auto;".  Without these options being set,
# the keys in this file are ignored.
#
# This file is NOT expected to be user-configured.
#
# These keys are current as of Feburary 2017.  If any key fails to
# initialize correctly, it may have expired.  In that event you should
# replace this file with a current version.  The latest version of
# bind.keys can always be obtained from ISC at https://www.isc.org/bind-keys.

managed-keys {
        # ISC DLV: See https://www.isc.org/solutions/dlv for details.
        #
        # NOTE: The ISC DLV zone is being phased out as of February 2017;
        # the key will remain in place but the zone will be otherwise empty.
        # Configuring "dnssec-lookaside auto;" to activate this key is
        # harmless, but is no longer useful and is not recommended.
        dlv.isc.org. initial-key 257 3 5 "BEAAAAPHMu/5onzrEE7z1egmhg/WPO0+juoZrW3euWEn4MxDCE1+lLy2
                brhQv5rN32RKtMzX6Mj70jdzeND4XknW58dnJNPCxn8+jAGl2FZLK8t+
                1uq4W+nnA3qO2+DL+k6BD4mewMLbIYFwe0PG73Te9fZ2kJb56dhgMde5
                ymX4BI/oQ+cAK50/xvJv00Frf8kw6ucMTwFlgPe+jnGxPPEmHAte/URk
                Y62ZfkLoBAADLHQ9IrS2tryAe7mbBZVcOwIeU/Rw/mRx/vwwMCTgNboM
                QKtUdvNXDrYJDSHZws3xiRXF1Rf+al9UmZfSav/4NWLKjHzpT59k/VSt
                TDN0YUuWrBNh";

        # ROOT KEYS: See https://data.iana.org/root-anchors/root-anchors.xml
        # for current trust anchor information.
        #
        # These keys are activated by setting "dnssec-validation auto;"
        # in named.conf.
        #
        # This key (19036) is to be phased out starting in 2017. It will
        # remain in the root zone for some time after its successor key
        # has been added. It will remain this file until it is removed from
        # the root zone.
        . initial-key 257 3 8 "AwEAAagAIKlVZrpC6Ia7gEzahOR+9W29euxhJhVVLOyQbSEW0O8gcCjF
                FVQUTf6v58fLjwBd0YI0EzrAcQqBGCzh/RStIoO8g0NfnfL2MTJRkxoX
                bfDaUeVPQuYEhg37NZWAJQ9VnMVDxP/VHL496M/QZxkjf5/Efucp2gaD
                X6RS6CXpoY68LsvPVjR0ZSwzz1apAzvN9dlzEheX7ICJBBtuA6G3LQpz
                W5hOA2hzCTMjJPJ8LbqF6dsV6DoBQzgul0sGIcGOYl7OyQdXfZ57relS
                Qageu+ipAdTTJ25AsRTAoub8ONGcLmqrAmRLKBP1dfwhYB4N7knNnulq
                QxA+Uk1ihz0=";

        # This key (20326) is to be published in the root zone in 2017.
        # Servers which were already using the old key (19036) should
        # roll seamlessly to this new one via RFC 5011 rollover. Servers
        # being set up for the first time can use the contents of this
        # file as initializing keys; thereafter, the keys in the
        # managed key database will be trusted and maintained
        # automatically.
        . initial-key 257 3 8 "AwEAAaz/tAm8yTn4Mfeh5eyI96WSVexTBAvkMgJzkKTOiW1vkIbzxeF3
                +/4RgWOq7HrxRixHlFlExOLAJr5emLvN7SWXgnLh4+B5xQlNVz8Og8kv
                ArMtNROxVQuCaSnIDdD5LKyWbRd2n9WGe2R8PzgCmr3EgVLrjyBxWezF
                0jLHwVN8efS3rCj/EWgvIWgb9tarpVUDK/b58Da+sqqls3eNbuv7pr+e
                oZG+SrDK6nWeL3c6H5Apxz7LjVc1uTIdsIXxuOLYA4/ilBmSVIzuDWfd
                RUfhHdY6+cn8HFRm+2hM8AnXGXws9555KrUB5qihylGa8subX2Nn6UwN
                R1AkUTV74bU=";
};
# END MANAGED KEYS

